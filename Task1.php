<?php
// Сreated a function that determines whether a word is a palindrome

function isPalindrome(string $word): bool
{
    $wordLength = mb_strlen($word);
    $wordMiddle = round($wordLength / 2);

    for ($i = 0; $i < $wordMiddle; $i++) {
        if ($word[$i] !== $word[$wordLength - 1 - $i]) {
            return false;
        }
    }

    return true;
}

echo isPalindrome("gffg") . PHP_EOL; //проверка работы функции со словом-палиндромом с четным количеством символов
echo isPalindrome("gfgfg") . PHP_EOL; //проверка работы функции со словом-палиндромом с нечетным количеством символов
echo isPalindrome("gffgf") . PHP_EOL; //проверка работы функции со словом, не являющимся палиндромом

