<?php
//Реализуйте функцию isBalanced, которая принимает на вход строку, состоящую только из открывающих
// и закрывающих круглых скобок, и проверяет, является ли эта строка корректной.
// Пустая строка (отсутствие скобок) считается корректной.
//
//Строка считается корректной (сбалансированной), если содержащаяся в ней скобочная структура соответствует
// требованиям:
//
//Скобки — это парные структуры. У каждой открывающей скобки должна быть соответствующая ей закрывающая скобка.
//Закрывающая скобка не должна идти впереди открывающей. Такой вариант недопустим )(, а вот такой допустим ()().

function isBalanced(string $str): bool
{
    $balanceBracketCounter = 0;

    for ($i = 0; $i < strlen($str); $i++) {
        $str[$i] === ')' ? $balanceBracketCounter-- : $balanceBracketCounter++;

        if ($balanceBracketCounter < 0) {
            return false;
        }
    }

    return $balanceBracketCounter == 0;
}

echo (isBalanced('()()') ? 'ok' : 'no ok') . '<br>'; //true
echo (isBalanced('(()()') ? 'ok' : 'no ok') . '<br>'; //false
echo (isBalanced(')()()(') ? 'ok' : 'no ok') . '<br>'; //false
echo (isBalanced('(()())') ? 'ok' : 'no ok') . '<br>'; //true
