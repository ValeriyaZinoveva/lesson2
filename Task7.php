<?php

//Реализуйте функцию binarySum, которая принимает на вход два бинарных числа (в виде строк) и возвращает их сумму.
// Результат (вычисленная сумма) также должен быть бинарным числом в виде строки.

function sumBin(string $firstBin, string $secondBin): string
{
    $firstInt = bindec($firstBin);
    $secondInt = bindec($secondBin);
    return decbin($firstInt + $secondInt);
}

echo sumBin('10', '1') . '<br>';
echo sumBin('1101', '101');
