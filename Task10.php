<?php
// Реализуйте функцию addDigits, которая принимает на вход неотрицательное целое число и возвращает другое число,
// полученное из первого следующим преобразованием: Итеративно сложите все входящие в него цифры до тех пор пока,
// не останется одна цифра.

function addDigits(int $number): int
{
    while (strlen((string)$number) > 1) {

        $sum = 0;
        $number = (string)$number;

        for ($i = 0; $i < strlen($number); $i++) {
            $sum = $sum + (int)$number[$i];
        }

        $number = $sum;
    }

    return $number;
}

echo addDigits(89) . '<br>'; // 8 + 9 = 17, 1 + 7 = 8
echo addDigits(4); // 4