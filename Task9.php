<?php

//Реализуйте функцию fib находящую положительные числа Фибоначчи.
// Аргументом функции является порядковый номер числа.

function fib(int $number): int
{
    return $number < 3 ? 1 : fib($number - 1) + fib($number - 2);
}

echo fib(1) . '<br>';//1
echo fib(10);//55