<?php

require_once 'Strings.php';

function isPalindrome (string $word): bool
{
    return $word === reverse($word) ;
}
