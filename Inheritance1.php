<?php

class User {
    protected string $name;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getRole(): string
    {
        return 'User';
    }
}

class Admin extends User {
    public function getName(): string
    {
        return "Admin " . parent::getName();
    }

    public function getRole(): string
    {
        return 'Admin';
    }
}

$firstUser = new User();
$firstUser->setName("Lera");

$admin = new Admin();
$admin->setName("Rinat");

echo $firstUser->getName() . '<br>';
echo $firstUser->getRole() . '<br>';

echo $admin->getName() . '<br>';
echo $admin->getRole() . '<br>';