<?php

//Реализуйте функцию isPowerOfThree которая определяет, является ли переданное
// число натуральной степенью тройки.
// Например, число 27 это третья степень, а 81 это четвертая.

function isPowerOfThree(int $number): bool
{
    return (int)log($number, 3) == log($number, 3);
}

echo isPowerOfThree(3) ? 'ok' : 'no ok';