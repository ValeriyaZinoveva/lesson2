<?php

class CheckTriangle
{
    protected int $side1;
    protected int $side2;
    protected int $side3;

    public function setSide1(int $side1): void
    {
        $this->side1 = $side1;
    }

    public function setSide2(int $side2): void
    {
        $this->side2 = $side2;
    }

    public function setSide3(int $side3): void
    {
        $this->side3 = $side3;
    }

    public function check(): bool
    {
        return $this->side1 > 0 && $this->side2 > 0 && $this->side3 > 0;
    }
}

class SuperCheck extends CheckTriangle
{

    public function checkExistenceTriangle(): bool
    {
        if ($this->side1 + $this->side2 <= $this->side3) {
            return false;
        }
        if ($this->side1 + $this->side3 <= $this->side2) {
            return false;
        }
        if ($this->side3 + $this->side2 <= $this->side1) {
            return false;
        }

        return true;
    }

    public function check(): bool
    {
        if (parent::check()) {
            return $this->checkExistenceTriangle();
        }
        return false;
    }
}

$triangle1 = new SuperCheck();
$triangle1->setSide1(2);
$triangle1->setSide2(5);
$triangle1->setSide3(6);

echo ($triangle1->check()) ? 'ok' : 'no ok';
