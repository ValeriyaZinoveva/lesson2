<?php

namespace lesson2\Number;

function reverse(int $number): int
{
     $strNumber = (string) abs($number);
     return $number >= 0 ? strrev($strNumber) : -strrev($strNumber);
}

echo reverse(-13); // -31
echo reverse(-123); // -321