<?php

namespace lesson2\Task6;

function swap(int &$firstVar, int &$secondVar): void
{
    $bufferVariable = $firstVar;
    $firstVar = $secondVar;
    $secondVar = $bufferVariable;
}

$first = 5;
$second = 8;

swap($first, $second);

echo $first;
echo $second;